# What's this?
これは、PowerShellモジュール配布用のリポジトリです。

## How to Setup?

need git.
need powershell ver5.1 latest.

```ps1
cd $env:PSModulePath.Split(":")[0]; git clone git@gitlab.com:iranika/wps.git
```

### Windows(PowerShell 5.0以降)

### Unix(Linux, MacOS, etc...)
そのうち書きます。

## Note
