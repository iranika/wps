function RepoUpdate {
    param(
        # Parameter help description
        [Parameter(Mandatory)][string]$RepoListFile
    )
    <#
    .SYNOPSIS
    
    .DESCRIPTION
    
    .EXAMPLE
    
    .EXAMPLE
    
    .NOTES

    .LINK
    Online version: http://github.com/iranika/wps
    #>
    $ModulesDir = $env:PSModulePath.Split(":")[0]
    $RepoList = &"$RepoListFile" # return type of list.
    $current = Get-Location
    foreach ($Repo in $RepoList){
        Set-Location $current
        $ModuleName = ($Repo -replace ".git", "") -replace "^.*/",""
        $ModulePath = Join-Path $ModulesDir $ModuleName
        if ((Test-Path $ModulePath) -eq $true){
            Set-Location $ModulePath
            git pull;
        }else{
            Set-Location $ModulesDir
            git clone $Repo
        }
    }
}
function WpsUpdate{
    param(
        [switch]$DayOnce
    )
        <#
    .SYNOPSIS
    
    .DESCRIPTION
    
    .EXAMPLE
    
    .EXAMPLE
    
    .NOTES

    .LINK        
    Online version: http://github.com/iranika/wps
    #>
    $here = Split-Path $MyInvocation.MyCommand.Module.Path -Parent 
    $ProfileDir = Split-Path $PROFILE -Parent
    $RepoListFile = "$here\RepoList.ps1"
    if ((Test-Path $RepoListFile) -eq $false) { Write-Output "return @()" > $RepoListFile }

    $current = Get-Location    
    if ($DayOnce -eq $false){
        RepoUpdate $RepoListFile
    }else{
        $LastPullDateFile = "$ProfileDir\LastPullDate.dat"
        if ((Test-Path $LastPullDateFile) -eq $false){ Write-Output "" > $LastPullDateFile }
        $last_pull_date = Get-Content -LiteralPath $LastPullDateFile
        $today = Get-Date -Format "yyyyMMdd"
        if ($last_pull_date -ne $today){
            $today > $LastPullDateFile
            RepoUpdate $RepoListFile
        }
    }
    Set-Location $current
}
#Export-ModuleMember -Function RepoUpdate
Export-ModuleMember -Function WpsUpdate
