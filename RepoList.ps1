<# this file set location in $PROFILE parent.
 # this script return repository url string list. example,
 # return @(
 #   "https://github.com/iranika/oftt.git"
 #   ,"https://github.com/iranika/oftt2.git" 
 # )
 #>
return @(
    "https://github.com/iranika/wps.git"
)